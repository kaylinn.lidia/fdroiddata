Categories:
  - Writing
License: MIT
WebSite: https://joplinapp.org/
SourceCode: https://github.com/laurent22/joplin
Changelog: https://joplinapp.org/changelog_android/
Donate: https://joplinapp.org/donate/

AutoName: Joplin
Description: |-
    Note: The F-Droid version does not have camera capabilities. Issues with this version might be raised on https://gitlab.com/fdroid/fdroiddata for user @muelli

    <strong>Joplin</strong> is a note taking and to-do application, which can handle a large number of notes organised into notebooks. The notes are searchable, can be copied, tagged and modified either from the applications directly or from your own text editor. The notes are in <a href="#markdown">Markdown format</a>.

    Notes exported from Evernote and other applications <a href="https://joplinapp.org/help/#importing">can be imported</a> into Joplin, including the formatted content (which is converted to Markdown), resources (images, attachments, etc.) and complete metadata (geolocation, updated time, created time, etc.). Plain Markdown files can also be imported.

    The notes can be securely <a href="https://joplinapp.org/help/#synchronisation">synchronised</a> using <a href="https://joplinapp.org/help/#encryption">end-to-end encryption</a> with various cloud services including Nextcloud, Dropbox, OneDrive and <a href="https://joplinapp.org/plans/">Joplin Cloud</a>.

    Full text search is available on all platforms to quickly find the information you need. The app can be customised using plugins and themes, and you can also easily create your own.

    The application is available for Windows, Linux, macOS, Android and iOS. A <a href="https://joplinapp.org/clipper/">Web Clipper</a>, to save web pages and screenshots from your browser, is also available for <a href="https://addons.mozilla.org/firefox/addon/joplin-web-clipper/">Firefox</a> and <a href="https://chrome.google.com/webstore/detail/joplin-web-clipper/alofnhikmmkdbbbgpnglcpdollgjjfek?hl=en-GB">Chrome</a>.

RepoType: git
Repo: https://github.com/laurent22/joplin.git

Builds:
  - versionName: 2.10.8
    versionCode: 2097684
    commit: 405c528ef00dee550dbf1586505c27bbff2d69e8
    subdir: packages/app-mobile/android/app
    sudo:
      - sysctl fs.inotify.max_user_watches=524288
      - apt-get update
      - apt-get install -y automake build-essential gobject-introspection gtk-doc-tools
        libexif-dev libexpat1-dev libgif-dev liblcms2-dev libpango1.0-dev libpng-dev
        libpoppler-glib-dev librsvg2-dev libtiff5-dev libtool python-is-python3
      - curl -Lo node.tar.gz https://nodejs.org/dist/v16.14.0/node-v16.14.0-linux-x64.tar.gz
      - echo "2c69e7b040c208b61ebf9735c63d2e5bcabfed32ef05a9b8dd5823489ea50d6b  node.tar.gz"
        | sha256sum -c -
      - tar xzf node.tar.gz --strip-components=1 -C /usr/local/
      - corepack enable
    init:
      - pushd ../../../..
      - yarn --version
      - YARN_ENABLE_IMMUTABLE_INSTALLS=false BUILD_SEQUENCIAL=1 yarn install
      - yarn remove --all react-native-camera
      - rm packages/app-mobile/components/CameraView.js
      - rm packages/app-mobile/components/screens/Note.js
    patch:
      - remove-camera.patch
      - 0001-android-remove-legacy-support.patch
    gradle:
      - yes
    srclibs:
      - react-native-sharp@v0.26.3
      - libvips@v8.10.0
    rm:
      - packages/tools/PortableAppsLauncher/
      - packages/app-cli
      - packages/plugin-repo-cli
      - packages/app-desktop
      - packages/app-clipper
      - packages/server
      - packages/app-mobile/components/CameraView.tsx
    prebuild:
      - pushd ../../../..
      - yarn --version
      - YARN_ENABLE_IMMUTABLE_INSTALLS=false BUILD_SEQUENCIAL=1 yarn install
      - sed -i '/android.permission.CAMERA/d' packages/app-mobile/android/app/src/main/AndroidManifest.xml
      - popd
      - sed -i -e '\!ACCESS_NETWORK_STATE!a<uses-permission android:name="com.android.vending.CHECK_LICENSE"
        tools:node="remove" />' -e 's!xmlns:android!xmlns:tools="http://schemas.android.com/tools"
        xmlns:android!' src/main/AndroidManifest.xml
      - sed -i '/:react-native-camera/{N;N;d}' build.gradle
      - pushd ../../../../packages/app-mobile/node_modules/react-native-rsa-native/android
      - sed -i -e 's|maven.fabric.io/public|jitpack.io|' build.gradle bin/build.gradle
      - popd
    scanignore:
      - packages/app-mobile/android/build.gradle
      - packages/app-mobile/node_modules/@react-native-community/netinfo/android/build.gradle
      - packages/app-mobile/node_modules/react-native-get-random-values/android/build.gradle
      - packages/app-mobile/node_modules/react-native-securerandom/android/build.gradle
      - packages/app-mobile/node_modules/react-native-vector-icons/android/build.gradle
      - packages/app-mobile/node_modules/react-native-rsa-native/android/build.gradle
      - packages/app-mobile/node_modules/react-native-webview/android/build.gradle
      - packages/app-mobile/node_modules/react-native-share/android/build.gradle
      - packages/app-mobile/node_modules/react-native/android
      - packages/app-mobile/node_modules/react-native/sdks/hermesc/linux64-bin/hermesc
      - packages/app-mobile/node_modules/jsc-android
      - packages/app-mobile/node_modules/react-native/ReactAndroid/build.gradle
      - packages/app-mobile/node_modules/react-native-rsa-native/android/bin/build.gradle
      - packages/app-mobile/node_modules/react-native-document-picker/android/build.gradle
      - packages/app-mobile/node_modules/react-native-safe-area-context/android/build.gradle
      - packages/app-mobile/node_modules/@react-native-community/datetimepicker/android/build.gradle
      - packages/react-native-alarm-notification/android/build.gradle
      - packages/react-native-saf-x/android/build.gradle
    scandelete:
      - packages/app-mobile/node_modules
      - packages/tools/node_modules
      - packages/lib/node_modules
      - packages/react-native-alarm-notification/node_modules
      - packages/react-native-saf-x/node_modules
      - node_modules
      - .yarn/cache/
      - .yarn/install-state.gz
    build:
      - pushd $$libvips$$
      - mkdir build
      - ./autogen.sh --prefix=$$libvips$$/build/
      - make -j$(nproc)
      - make install
      - popd
      - pushd $$react-native-sharp$$
      - export PKG_CONFIG_PATH=$$libvips$$/build/lib/pkgconfig/
      - npm install --build-from-source
      - popd

  - versionName: 2.10.9
    versionCode: 2097685
    commit: a0f582b2b9a7dfecf35705601f6ae7e12902d834
    subdir: packages/app-mobile/android/app
    sudo:
      - sysctl fs.inotify.max_user_watches=524288
      - apt-get update
      - apt-get install -y automake build-essential gobject-introspection gtk-doc-tools
        libexif-dev libexpat1-dev libgif-dev liblcms2-dev libpango1.0-dev libpng-dev
        libpoppler-glib-dev librsvg2-dev libtiff5-dev libtool python-is-python3
      - curl -Lo node.tar.gz https://nodejs.org/dist/v16.14.0/node-v16.14.0-linux-x64.tar.gz
      - echo "2c69e7b040c208b61ebf9735c63d2e5bcabfed32ef05a9b8dd5823489ea50d6b  node.tar.gz"
        | sha256sum -c -
      - tar xzf node.tar.gz --strip-components=1 -C /usr/local/
      - corepack enable
    init:
      - pushd ../../../..
      - yarn --version
      - YARN_ENABLE_IMMUTABLE_INSTALLS=false BUILD_SEQUENCIAL=1 yarn install
      - yarn remove --all react-native-camera
      - rm packages/app-mobile/components/CameraView.js
      - rm packages/app-mobile/components/screens/Note.js
    patch:
      - remove-camera.patch
      - 0001-android-remove-legacy-support.patch
    gradle:
      - yes
    srclibs:
      - react-native-sharp@v0.26.3
      - libvips@v8.10.0
    rm:
      - packages/tools/PortableAppsLauncher/
      - packages/app-cli
      - packages/plugin-repo-cli
      - packages/app-desktop
      - packages/app-clipper
      - packages/server
      - packages/app-mobile/components/CameraView.tsx
    prebuild:
      - pushd ../../../..
      - yarn --version
      - YARN_ENABLE_IMMUTABLE_INSTALLS=false BUILD_SEQUENCIAL=1 yarn install
      - sed -i '/android.permission.CAMERA/d' packages/app-mobile/android/app/src/main/AndroidManifest.xml
      - popd
      - sed -i -e '\!ACCESS_NETWORK_STATE!a<uses-permission android:name="com.android.vending.CHECK_LICENSE"
        tools:node="remove" />' -e 's!xmlns:android!xmlns:tools="http://schemas.android.com/tools"
        xmlns:android!' src/main/AndroidManifest.xml
      - sed -i '/:react-native-camera/{N;N;d}' build.gradle
      - pushd ../../../../packages/app-mobile/node_modules/react-native-rsa-native/android
      - sed -i -e 's|maven.fabric.io/public|jitpack.io|' build.gradle bin/build.gradle
      - popd
    scanignore:
      - packages/app-mobile/android/build.gradle
      - packages/app-mobile/node_modules/@react-native-community/netinfo/android/build.gradle
      - packages/app-mobile/node_modules/react-native-get-random-values/android/build.gradle
      - packages/app-mobile/node_modules/react-native-securerandom/android/build.gradle
      - packages/app-mobile/node_modules/react-native-vector-icons/android/build.gradle
      - packages/app-mobile/node_modules/react-native-rsa-native/android/build.gradle
      - packages/app-mobile/node_modules/react-native-webview/android/build.gradle
      - packages/app-mobile/node_modules/react-native-share/android/build.gradle
      - packages/app-mobile/node_modules/react-native/android
      - packages/app-mobile/node_modules/react-native/sdks/hermesc/linux64-bin/hermesc
      - packages/app-mobile/node_modules/jsc-android
      - packages/app-mobile/node_modules/react-native/ReactAndroid/build.gradle
      - packages/app-mobile/node_modules/react-native-rsa-native/android/bin/build.gradle
      - packages/app-mobile/node_modules/react-native-document-picker/android/build.gradle
      - packages/app-mobile/node_modules/react-native-safe-area-context/android/build.gradle
      - packages/app-mobile/node_modules/@react-native-community/datetimepicker/android/build.gradle
      - packages/react-native-alarm-notification/android/build.gradle
      - packages/react-native-saf-x/android/build.gradle
    scandelete:
      - packages/app-mobile/node_modules
      - packages/tools/node_modules
      - packages/lib/node_modules
      - packages/react-native-alarm-notification/node_modules
      - packages/react-native-saf-x/node_modules
      - node_modules
      - .yarn/cache/
      - .yarn/install-state.gz
    build:
      - pushd $$libvips$$
      - mkdir build
      - ./autogen.sh --prefix=$$libvips$$/build/
      - make -j$(nproc)
      - make install
      - popd
      - pushd $$react-native-sharp$$
      - export PKG_CONFIG_PATH=$$libvips$$/build/lib/pkgconfig/
      - npm install --build-from-source
      - popd

MaintainerNotes: |-
    * "g++" is needed for a dependency built by "npm install".
    * "scanignore" of "build.gradle" are for local Maven repositories, the React Native folder
      is for "react-native-0.66.1.aar", and the JSC Android folder for "android-jsc-intl-r241213.jar".
    - we build libvips from source for the sharp library

AutoUpdateMode: Version
UpdateCheckMode: Tags android-.*
CurrentVersion: 2.10.9
CurrentVersionCode: 2097685
